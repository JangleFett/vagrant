# Create a new instance of the latest Ubuntu 14.04 on an
# t2.micro node with an AWS Tag naming it "HelloWorld"
provider "aws" {
  region = "eu-central-1"
}

variable "vpcid" {
  type = "string"
  description = "VPC ID to place VM"
  default = "vpc-ed6ab384"
}

data "aws_ami" "centos" {
  most_recent = true

  filter {
    name   = "name"
    values = ["dcos-centos7-201607072229"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["273854932432"]
}

resource "aws_security_group" "websg" {
  name = "vm_websg"
  description = "Allow ports 22 and 80"
  vpc_id = "${var.vpcid}"

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "SteveTestTFSG"
  }
}

resource "aws_instance" "web" {
  ami           = "${data.aws_ami.centos.id}"
  key_name = "steveshilling"
  instance_type = "t2.micro"
  vpc_security_group_ids = ["${aws_security_group.websg.id}"]
  user_data = "${file("build.sh")}"

#  provisioner "remote-exec" {
#    inline = [
#      "sudo yum -y update",
#      "sudo yum -y install httpd",
#      "echo '<h1>Did the Earth move for you?</h1>' >/tmp/index.html",
#      "sudo mv /tmp/index.html /var/www/html",
#      "sudo chkconfig httpd on",
#      "sudo service httpd start"
#    ]
#
#    connection {
#      type = "ssh"
#      user = "ec2-user"
#      private_key = "${file("~/.ssh/id_rsa")}"
#    }
#  }

  tags {
    Name = "SteveTestTF"
  }
}
