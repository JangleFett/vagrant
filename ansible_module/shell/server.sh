#!/bin/bash

yum -y install ansible

cp /vagrant/secrets/academy_jan_2018.pem /home/vagrant/.ssh/id_rsa

echo "Host *
  StrictHostKeyChecking no" >/home/vagrant/.ssh/config

chmod 600 /home/vagrant/.ssh/*
chown vagrant:vagrant /home/vagrant/.ssh/*
