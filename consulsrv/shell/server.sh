#!/bin/bash

# Install docker software from community
yum -y install  yum-utils  device-mapper-persistent-data lvm2

# Set up docker software repository to allow install of docker software
yum-config-manager -y --add-repo https://download.docker.com/linux/centos/docker-ce.repo

# Install Docker community edition
yum -y install docker-ce

# systemctl start docker
service docker start

# systemctl enable docker
chkconfig docker on

# Add vagrant user to docker group
usermod -G docker vagrant

myRealIP=$(ifconfig | grep -A6 enp0s9 | grep 'inet ' | awk '{print $2}')
dockerIP=$(ifconfig | grep -A6 docker0 | grep 'inet ' | awk '{print $2}')

# Start a consul server
docker run -d -h consulsrv -v /mnt:/data \
-p $myRealIP:8300:8300 \
-p $myRealIP:8301:8301 \
-p $myRealIP:8301:8301/udp \
-p $myRealIP:8302:8302 \
-p $myRealIP:8302:8302/udp \
-p $myRealIP:8400:8400 \
-p $myRealIP:8500:8500 \
-p $dockerIP:53:53/udp \
progrium/consul -server -advertise $myRealIP -bootstrap
