#!/bin/bash

cd /vagrant/ansible

if [[ -e /etc/redhat-release ]]
then
  yum -y install ansible
  ansible-playbook -i inventory apache.yml
else
  apt-get -y update
  apt-get -y install ansible sshpass
  ansible-playbook -i inventory nginx2.yml
fi

# Test our page worked
if curl -s http://localhost/status.html | grep ANSIBLE >/dev/null 2>&1
then
  echo "Web page working correctly"
else
  echo "Failed to get web page"
  exit 1
fi
