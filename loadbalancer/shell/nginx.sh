#!/bin/bash

apt-get -y update
apt-get -y install nginx
update-rc.d nginx defaults
service nginx start

cp /vagrant/files/webscript /etc/init.d
chmod +x /etc/init.d/webscript
update-rc.d webscript defaults
service webscript start
# systemctl start webscript
